﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGenerator : MonoBehaviour
{

    public GameObject[] greensPrefab;
    public GameObject[] roadsPrefab;
    public GameObject waterPrefab;
    public GameObject railPrefab;

    List<GameObject> gameMap;
    int currentStep;
    int needToGenerate;

    TypeOfBlock lastTypeOfBlock;
    enum TypeOfBlock
    {
        Green, Road, Water, Rail
    }


    private void Start()
    {
        needToGenerate = 0;
        gameMap = new List<GameObject>();
        currentStep = 10;

        needToGenerate = 30;
        FirstGeneration(10);

        Initialization();
    }


    private void Initialization()
    {
        while (needToGenerate >= 0) GenerateMap();
    }

    private void FirstGeneration(int size)
    {
        for (int i = 0; i < size; i++)
        {
            if (i % 2 == 0) SpawnObject(greensPrefab[0]);
            else SpawnObject(greensPrefab[1]);
        }
    }


    public void GenerateBlock()
    {
        needToGenerate++;
        RemoveLastItme();
        if (needToGenerate > 0) GenerateMap();
    }

    private void RemoveLastItme()
    {
        gameMap[0].GetComponent<EnvironmentGenerator>().ReturnToPool();
        gameMap.RemoveAt(0);
    }

       
    private void GenerateMap()
    {
        TypeOfBlock rand = TypeOfBlock.Green;
        if (lastTypeOfBlock == TypeOfBlock.Water && Random.value >= 0.15f) GenerateGreen();
        else
        {
            float random = Random.value;
            if (random < 0.3f) rand = TypeOfBlock.Green;
            else if (random > 0.3f && random < 0.6f) rand = TypeOfBlock.Road;
            else if (random > 0.6f && random < 0.8f) rand = TypeOfBlock.Rail;
            else rand = TypeOfBlock.Water;

            if (rand == lastTypeOfBlock)
            {
                GenerateMap();
                return;
            }
            switch (rand)
            {
                case TypeOfBlock.Green: GenerateGreen(); break;
                case TypeOfBlock.Road: GenerateRoad(); break;
                case TypeOfBlock.Water: GenerateWater(); break;
                case TypeOfBlock.Rail: GenerateRail(); break;
            }
        }
    }

    private void GenerateGreen()
    {
        lastTypeOfBlock = TypeOfBlock.Green;
        int size = Random.Range(1, 4);
        for(int i = 0; i < size; i++)
        {
            if (i % 2 == 0) SpawnObject(greensPrefab[0]);
            else SpawnObject(greensPrefab[1]);
        }
    }

    private void GenerateRail()
    {
        lastTypeOfBlock = TypeOfBlock.Rail;
        int size = Random.Range(1, 3);
        for (int i = 0; i < size; i++)
        {
            SpawnObject(railPrefab);
        }
    }

    private void GenerateWater()
    {
        lastTypeOfBlock = TypeOfBlock.Water;
        int size = Random.Range(1, 4);
        for (int i = 0; i < size; i++)
        {
            if(i % 2 == 0) SpawnObject(waterPrefab, new Vector3(0,180,0));
            else SpawnObject(waterPrefab, new Vector3(0, 0, 0));
        }
    }

    private void GenerateRoad()
    {
        lastTypeOfBlock = TypeOfBlock.Road;
        int size = Random.Range(1, 6);
        for (int i = 0; i < size; i++)
        {
            if (size == 1) SpawnObject(roadsPrefab[0]);
            else if (i == 0) SpawnObject(roadsPrefab[1]);
            else if (i == (size - 1)) SpawnObject(roadsPrefab[1], new Vector3(0, 180, 0));
            else SpawnObject(roadsPrefab[2]);
        }
    }

    private void SpawnObject(GameObject prefab)
    {
        gameMap.Add(PoolManager.GetObject(prefab.name, new Vector3(currentStep, 0, 0), Vector3.zero));
        gameMap[gameMap.Count - 1].GetComponent<EnvironmentGenerator>().GenerateEnvironment();
        currentStep--;
        needToGenerate--;
    }
    private void SpawnObject(GameObject prefab, Vector3 rotation)
    {
        gameMap.Add(PoolManager.GetObject(prefab.name, new Vector3(currentStep, 0, 0), rotation));
        gameMap[gameMap.Count - 1].GetComponent<EnvironmentGenerator>().GenerateEnvironment();
        currentStep--;
        needToGenerate--;
    }

}
