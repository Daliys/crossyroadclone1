﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenGenerator : EnvironmentGenerator
{
    public GameObject[] environmentObjects;

    public override void GenerateEnvironment()
    {
        int rand = Random.Range(2, 5);
        for (int i = 0; i < rand; i++)
        {
            GenerateObject(new Vector2(Game.movementAvalible.z + 1, (int)(Game.groundSizeZ / 2) + 1));
            GenerateObject(new Vector2(-(int)(Game.groundSizeZ / 2), -Game.movementAvalible.z - 1));
        }

        rand = Random.Range(0, 4);
        for (int i = 0; i < rand; i++)
        {
            GenerateObject(new Vector2(-Game.movementAvalible.z , Game.movementAvalible.z + 1));
        }
        
        if (Random.value < Game.dropProbabilityOfCoin ) GenerateCoin();

    }

  

    private void GenerateObject(Vector2 range)
    {
        int randPosition = Random.Range((int)range.x, (int)range.y);
        if (randPosition == 0) if (Random.value > 0.1)
            {
                GenerateObject(range);
                return;
            }
        

        foreach (var item in environmentList)
        {
            if (item.transform.position.z == randPosition)
            {
                GenerateObject(range);
                return;
            }
        }

        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 1, randPosition);
        GameObject gm = PoolManager.GetObject(environmentObjects[Random.Range(0, environmentObjects.Length)].name, pos, Vector3.zero);
        environmentList.Add(gm);

    }

 

}
