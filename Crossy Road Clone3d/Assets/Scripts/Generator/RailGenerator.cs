﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailGenerator : EnvironmentGenerator
{
    public GameObject train;
    public Animator animator;
    public AudioSource audioSource;

    private float sizeVehicle;

    private float range;
    private float timeWaiting;
    private float timeBeforeFirstStating;
    private float speed;

    protected override void Awake()
    {
        base.Awake();
        sizeVehicle = train.GetComponent<BoxCollider>().size.z;
        range = Game.groundSizeZ / 2 + sizeVehicle / 2;
        timeWaiting = Random.Range(3, 8);
        timeBeforeFirstStating = Random.Range(1, 3);
        speed = Random.Range(1,4);
    }
  
    public override void GenerateEnvironment()
    {

        bool isDirectionLeft = Random.value > Random.value;
        
        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 1, Game.groundSizeZ/2+sizeVehicle/2);
        GameObject gm = PoolManager.GetObject(train.name, pos, Vector3.zero);

        gm.GetComponent<TransportMovement>().StartMovement(range, -range, isDirectionLeft, speed,timeWaiting , timeBeforeFirstStating);
        StartCoroutine(TraficLightAnim(timeWaiting+speed, timeBeforeFirstStating-1));

        environmentList.Add(gm);

        if (Random.value < Game.dropProbabilityOfCoin) GenerateCoin();
    }

    IEnumerator TraficLightAnim(float timeWaiting, float delayBeforeStarting)
    {
        yield return new WaitForSeconds(delayBeforeStarting);
        while (true)
        {
            animator.SetTrigger("IsTrain");
            audioSource.Play();
            yield return new WaitForSeconds(timeWaiting);
        }
    }

    public override void ReturnToPool()
    {
        StopAllCoroutines();
        base.ReturnToPool();
    }


}
