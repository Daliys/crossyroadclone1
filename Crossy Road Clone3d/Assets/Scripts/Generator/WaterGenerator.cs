﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGenerator : EnvironmentGenerator
{
    public Obstacle[] woods;
    public GameObject lily;

    private float distanceBetweenObstacle =2f;

    [System.Serializable]
    public struct Obstacle
    {
        public GameObject prefab;
        public int size;
    }
    

    public override void GenerateEnvironment()
    {
        bool isDirectionLeft = transform.rotation.y == 0 ? true : false;

        float range = Game.groundSizeZ / 2;
        float obstacleSpeed = Random.Range(6, 12);
        float delay = Random.Range(0, 1);

        if (Random.value > 0.3)
        {
            for (int i = 0; i < Random.Range(3, 4); i++)
            {
                int randObstacle = Random.Range(0, woods.Length);
                Vector3 pos = new Vector3(transform.position.x, transform.position.y, range);
                GameObject gm = PoolManager.GetObject(woods[randObstacle].prefab.name, pos, Vector3.zero);
                gm.GetComponent<TransportMovement>().StartMovement(range, -range, GetRandomPosition(range, woods[randObstacle].size), isDirectionLeft, obstacleSpeed, delay);
                environmentList.Add(gm);

            }
        }
        else
        {
            for (int i = 0; i < Random.Range(3, 4); i++)
            {
               
                Vector3 pos = new Vector3(transform.position.x, transform.position.y, GetRandomPosition(Game.movementAvalible.z));
                GameObject gm = PoolManager.GetObject(lily.name, pos, Vector3.zero);
                environmentList.Add(gm);

            }
        }
        
    }

    public float GetRandomPosition(float range, float size)
    {
        float random = Random.Range(-range, range);
        float sumDistance = size / 2 + distanceBetweenObstacle;
        foreach (var item in environmentList)
        {
            if (random < item.transform.position.z - sumDistance || random > item.transform.position.z + sumDistance) continue;
            else return GetRandomPosition(range, size);
        }

        return random;
    }

    public float GetRandomPosition(float range)
    {
        int random = Random.Range(-(int)range, (int)range + 1);
        foreach(var item in environmentList)
        {
            if (item.transform.position.z == random) GetRandomPosition(range);
        }

        return random;
    }



}
