﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour
{

    List<PoolObject> objects;

    public void Initialization(GameObject prefab, int count)
    {
        objects = new List<PoolObject>();

        for (int i = 0; i < count; i++)
        {
            AddObject(prefab);

        }
    }

    private void AddObject(GameObject prefab)
    {
        GameObject gm = Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity, transform);
        gm.name = (prefab.name);
        objects.Add(gm.GetComponent<PoolObject>());
        gm.SetActive(false);
    }

    public GameObject GetObject()
    {
        foreach (var item in objects)
        {
            if (!item.gameObject.activeInHierarchy) return item.gameObject;
        }

        AddObject(objects[0].gameObject);
        return objects[objects.Count - 1].gameObject;
    }


}
