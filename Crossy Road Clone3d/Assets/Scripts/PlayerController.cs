﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    public GameGenerator gameGenerator;
    public UIManager UIManager;

    float lastMaxPositionX;
    bool isJump;
    float jumpSpeed;
    bool isOnTheGround;
    bool isMoveUp;
    Animator animator;
    Transform followTo;
    float differenceFollowingZ;

    private Touch android_theTouch;
    private Vector2 android_touchStartPosition, android_touchEndPosition;
    private Vector3 android_direction;

    void Start()
    {
        animator = transform.GetComponent<Animator>();
        followTo = null;
        jumpSpeed = 0.05f;
        lastMaxPositionX = transform.position.x;
        StartCoroutine(TimerForDeath());
    }
    // Update is called once per frame
    void Update()
    {
        if (followTo != null)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, followTo.transform.position.z - differenceFollowingZ);
            if (transform.position.z > Game.movementAvalible.z || transform.position.z < -Game.movementAvalible.z)
            {
                Game.IsGameOver = true;
                UIManager.ShowGameOverPanel();
            }
            if (transform.position.z >= Game.groundSizeZ / 2 - 1 || transform.position.z <= -Game.groundSizeZ / 2 + 1) followTo = null;
        }

        if (Game.IsGameOver || Game.isPause) return;

#if UNITY_EDITOR
        UnityControll();
#elif UNITY_ANDROID
        AndroidControll();
#endif
    }
    private void AndroidControll()
    {
        if (Input.touchCount > 0)
        {
            android_theTouch = Input.GetTouch(0);

            if (android_theTouch.phase == TouchPhase.Began)
            {
                android_touchStartPosition = android_theTouch.position;
            }

            else if ( android_theTouch.phase == TouchPhase.Ended)
            {
                android_touchEndPosition = android_theTouch.position;

                float x = android_touchEndPosition.x - android_touchStartPosition.x;
                float y = android_touchEndPosition.y - android_touchStartPosition.y;
                android_direction = Vector3.zero;

                if (Mathf.Abs(x) == 0 && Mathf.Abs(y) == 0)
                {
                    if (!isJump && isOnTheGround)
                        android_direction = Vector3.forward;
                }

                else if (Mathf.Abs(x) > Mathf.Abs(y))
                {
                    if (!isJump && isOnTheGround)
                        if (x > 0 && Mathf.Abs(transform.position.z - Game.movementAvalible.z) > 0.01f) android_direction = Vector3.right;
                        else if (Mathf.Abs(transform.position.z + Game.movementAvalible.z) > 0.01f) android_direction = Vector3.left;
                }

                else
                {
                    if (!isJump && isOnTheGround)
                        if (y > 0) android_direction = Vector3.forward;
                        else if (transform.position.x < lastMaxPositionX + Game.movementAvalible.x) android_direction = Vector3.back;
                }
                ControllCharacter(android_direction);
            }
        }
    }

    private void UnityControll()
    {
        if ((Input.GetKeyDown(KeyCode.Space) || (Input.GetKeyDown(KeyCode.W))) && !isJump && isOnTheGround)
        {
            ControllCharacter(Vector3.forward);
        }
        if (Input.GetKeyDown(KeyCode.D) && !isJump && isOnTheGround && (Mathf.Abs(transform.position.z - Game.movementAvalible.z) > 0.01f))
        {
            ControllCharacter(Vector3.right);
        }
        if (Input.GetKeyDown(KeyCode.A) && !isJump && isOnTheGround && (Mathf.Abs(transform.position.z + Game.movementAvalible.z) > 0.01f))
        {
            ControllCharacter(Vector3.left);
        }
        if (Input.GetKeyDown(KeyCode.S) && !isJump && isOnTheGround && (transform.position.x < lastMaxPositionX + Game.movementAvalible.x))
        {
            ControllCharacter(Vector3.back);
        }
    }

    public void ControllCharacter(Vector3 direction)
    {
        RaycastHit hit;
        Vector3 jump = Vector3.zero;
        Vector3 rotate = Vector3.zero;
       
        if (direction == Vector3.forward)
        {
            if (Physics.Raycast(transform.position, Vector3.left, out hit, 1))
            {
                if (hit.collider.tag == "Obstacle") return;
            }
            jump = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z);
            rotate = transform.rotation.y == 0 ? transform.rotation.eulerAngles : new Vector3(transform.rotation.x, 0, transform.rotation.z);
          
        }
        else if (direction == Vector3.right)
        {
            if (Physics.Raycast(transform.position, Vector3.forward, out hit, 1))
            {
                if (hit.collider.tag == "Obstacle") return;
            }
            jump = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
            rotate = transform.rotation.y == 90 ? transform.rotation.eulerAngles : new Vector3(transform.rotation.x, +90, transform.rotation.z);
        }
        else if (direction == Vector3.left)
        {
            if (Physics.Raycast(transform.position, Vector3.back, out hit, 1))
            {
                if (hit.collider.tag == "Obstacle") return;
            }
            jump = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
            rotate = transform.rotation.y == -90 ? transform.rotation.eulerAngles : new Vector3(transform.rotation.x, -90, transform.rotation.z);
        }
        else if (direction == Vector3.back)
        {
            if (Physics.Raycast(transform.position, Vector3.right, out hit, 1))
            {
                if (hit.collider.tag == "Obstacle") return;
            }
            jump = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
            rotate = transform.rotation.y == 180 ? transform.rotation.eulerAngles : new Vector3(transform.rotation.x, 180, transform.rotation.z);
        }
        else return;

        transform.DOJump(jump, 1, 1, jumpSpeed, true).OnComplete(CompleateMoveUp).Join(transform.DORotate(rotate, jumpSpeed, RotateMode.Fast));
        isJump = true;
        isOnTheGround = false;
    }

    IEnumerator TimerForDeath()
    {

        float elapsedTime = 0;

        while (elapsedTime <= Game.timeToDeath)
        {
            elapsedTime += Time.deltaTime;
            if (isMoveUp || Game.isPause)
            {
                elapsedTime = 0;
                isMoveUp = false;
            }

            yield return null;
        }
        if (!Game.IsGameOver)
        {
           
            animator.SetTrigger("IsDeathTime");
            Game.IsGameOver = true;
            UIManager.ShowGameOverPanel();
        }
    }

    public void CompleateMoveUp()
    {
        isJump = false;

        if (transform.position.x < lastMaxPositionX)
        {
            lastMaxPositionX = transform.position.x;
            isMoveUp = true;
            Game.currentSteps++;
            gameGenerator.GenerateBlock();
            UIManager.UpdateUISteps();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Floor")
        {
            isOnTheGround = true;
        }
        if (collision.transform.tag == "Wood")
        {
            isOnTheGround = true;
            followTo = collision.transform;
            differenceFollowingZ = followTo.transform.position.z - transform.position.z;
        }
        if (collision.transform.tag == "GameOver") transform.localScale = new Vector3(transform.localScale.x, 0.2f, transform.localScale.z);
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Wood")
        {
            followTo = null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Danger")
        {
            if(!Game.IsGameOver) animator.SetTrigger("IsDeathVehicleSide");
            Game.IsGameOver = true;
            UIManager.ShowGameOverPanel();
          
        }
        if (other.tag == "Coin")
        {
            Game.coins++;
            UIManager.UpdateUICoins();
            other.gameObject.GetComponent<PoolObject>().ReturnToPool();
        }
    }

}
