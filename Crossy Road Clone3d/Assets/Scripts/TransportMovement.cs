﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class TransportMovement : MonoBehaviour
{
    float startPositionZ = 23;
    float endPositionZ = -23;
    float speed;
    float delayRestart;

    public Ease ease;
 
    bool isDotweenActive;
 
    Tween tween;


    void Awake()
    {
        //tween = transform.DOMoveZ(endPositionZ, speed, false).SetEase(ease).SetDelay(delayRestart).OnComplete(Repeat);
    }

    private void Update()
    {
       
    }

    private void Repeat()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, startPositionZ);

        if (isDotweenActive)
        {
            tween = transform.DOMoveZ(endPositionZ, speed, false).SetEase(ease).SetDelay(delayRestart).OnComplete(Repeat);
        }
    }
    
    public void StartMovement(float startPosition, float endPosition, float currentPos ,bool isDirectionLeft, float speed, float delayRestart)
    {
        isDotweenActive = true;
        this.speed = speed;
        this.delayRestart = delayRestart;
        if (isDirectionLeft)
        {
            startPositionZ = startPosition;
            endPositionZ = endPosition;
            transform.eulerAngles = new Vector3(transform.rotation.x, 180, transform.rotation.z);
        }
        else
        {
            startPositionZ = endPosition;
            endPositionZ = startPosition;
            transform.eulerAngles = new Vector3(transform.rotation.x, 0, transform.rotation.z);
        }

        speed = (speed/(Mathf.Abs(endPosition-startPosition))) * (Mathf.Abs(endPositionZ - currentPos)) ;
        transform.position = new Vector3(transform.position.x, transform.position.y, currentPos);
     
        tween = transform.DOMoveZ(endPositionZ, speed, false).SetEase(ease).OnComplete(Repeat);//.SetLoops(-1));
    }

    public void StartMovement(float startPosition, float endPosition, bool isDirectionLeft, float speed, float delayRestart, float delayBeforFirstStarting)
    {
        isDotweenActive = true;
        this.speed = speed;
        this.delayRestart = delayRestart;

        if (isDirectionLeft)
        {
            startPositionZ = startPosition;
            endPositionZ = endPosition;
            transform.eulerAngles = new Vector3(transform.rotation.x, 180, transform.rotation.z);
        }
        else
        {
            startPositionZ = endPosition;
            endPositionZ = startPosition;
            transform.eulerAngles = new Vector3(transform.rotation.x, 0, transform.rotation.z);
        }
        transform.position = new Vector3(transform.position.x, transform.position.y, startPositionZ);

        tween = transform.DOMoveZ(endPositionZ, speed, false).SetEase(ease).SetDelay(delayBeforFirstStarting).OnComplete(Repeat);
    }


    private void OnDisable()
    {
        tween.Kill(false);
    }


}
