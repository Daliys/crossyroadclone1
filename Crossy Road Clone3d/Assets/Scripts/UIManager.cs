﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public Animator animatorSound;
    public GameObject ganeOverPanel;
    public GameObject startPanel;
    public GameObject gamePanel;

    public Text coinText;
    public Text stepsText;


    private bool isSound;

    void Start()
    {
        UpdateUICoins();
        UpdateStepRecord();
        isSound = Game.GetSavingData("IsSound", 0) == 1 ? true : false;
        AudioListener.pause = isSound;
        animatorSound.SetBool("IsSound", isSound);
    }


    public void UpdateStepRecord()
    {
        stepsText.text = Game.recordSteps.ToString();
    }
    public void SoundButton()
    {
        isSound = !isSound;
        animatorSound.SetBool("IsSound", isSound);
        AudioListener.pause = isSound;
        Game.SavingData("IsSound", isSound? 1 : 0);
    }

    public void UpdateUISteps()
    {
        stepsText.text = Game.currentSteps.ToString();
    }

    public void UpdateUICoins()
    {
        coinText.text = Game.coins.ToString();
    }

    public void ShowGameOverPanel()
    {
        ganeOverPanel.SetActive(true);
        startPanel.SetActive(false);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    public void StartGameButton()
    {
        startPanel.SetActive(false);
        Game.isPause = false;
        Game.IsGameOver = false;
        Game.currentSteps = 0;
        UpdateUISteps();
    }
}
