﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarsHorn : MonoBehaviour
{

    public AudioSource audioSource;

    void Start()
    {
        if(Random.value > 0.9)StartCoroutine(Horne(Random.Range(2,8)));
    }


    IEnumerator Horne(float time)
    {
  
            yield return new WaitForSeconds(time);
            audioSource.Play();

    }

}
