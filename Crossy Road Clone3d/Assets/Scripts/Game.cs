﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{

    public static bool IsGameOver { 
        get { return isGameOver; } 
        set { if (value) SetGameOver();
            isGameOver = value; }
    }

    private static bool isGameOver;
    public static bool isPause;

    public static float groundSizeZ = 23;
    public static Vector3 movementAvalible = new Vector3(2,0,4);
    public static float dropProbabilityOfCoin = 0.05f;
    public static float timeToDeath = 5f;

    public static int coins;
    public static int currentSteps;
    public static int recordSteps;


    public static void SetGameOver()
    {
        SavingData("coins", coins);
        if (currentSteps > recordSteps)
        {
            SavingData("recSteps", currentSteps);
            recordSteps = currentSteps;
        }
    }

    private void Awake()
    {
        coins = GetSavingData("coins", 0);
        recordSteps = GetSavingData("recSteps", 0);
        isPause = true;
      

    }

    public static int GetSavingData(string keyName, int value)
    {
        if (!PlayerPrefs.HasKey(keyName)) PlayerPrefs.SetInt(keyName, value);
        else value = PlayerPrefs.GetInt(keyName);
        return value;
    }

    public static void SavingData(string keyName, int value)
    {
        PlayerPrefs.SetInt(keyName, value);
    }


}
